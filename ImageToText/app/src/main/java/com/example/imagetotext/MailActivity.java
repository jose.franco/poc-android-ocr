package com.example.imagetotext;


import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.imagetotext.Cortar;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MailActivity extends AppCompatActivity {


    TextView text;
    EditText mail;
    Button enviar;
    String correo;
    String contraseña;
    String datos;


    Session session;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mail);
        text= (TextView) findViewById(R.id.texto);
        mail=(EditText)findViewById(R.id.mail);
        datos = getIntent().getStringExtra("texto");
        Cortar c = new Cortar(datos);
        text.setText(c.resultado());
        enviar= (Button) findViewById(R.id.enviar);


        correo = "pruebas.frida@hotmail.com";
        contraseña ="frida1234";
        enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), SendActivity.class);

                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);

                Properties properties=new Properties();
                properties.put( "mail.transport.protocol", "smtp" );
                properties.put("mail.smpt.host","smtp.live.com");
                properties.put("mail.smpt.auth","true");
                properties.put("mail.smtp.starttls.enable", "true");



                try {
                    session = Session.getDefaultInstance(properties, new Authenticator() {
                        @Override
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(correo, contraseña);
                        }
                    });

                    String destino= text.getText().toString();

                    if(session !=null){
                        Cortar c = new Cortar(datos);
                        Message message = new MimeMessage(session);
                        message.setFrom( new InternetAddress(correo));
                        message.setSubject("Softtek Informa");
                        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mail.getText().toString()));
                        message.setContent(c.resultado(),"text/html; charset=utf-8");
                        Transport transport= session.getTransport("smtp");
                        transport.connect("smtp.live.com",587,correo,contraseña );
                        transport.sendMessage(message,message.getAllRecipients());
                        transport.close();
                        startActivity(i);

                    }
                }catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

    public void continuar(View view) {
        Intent i = new Intent(this, SendActivity.class);
        startActivity(i);
    }

}

