package com.example.imagetotext;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MenuActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }

    public void EscanderQR(View view){
        Intent i = new Intent(this, QrActivity.class);
        startActivity(i);
    }
    public void EscanderOCR(View view){
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }

    public void CamaraAbierta(View view){
        Intent i = new Intent(this, CamaraActivity.class);
        startActivity(i);
    }
}

