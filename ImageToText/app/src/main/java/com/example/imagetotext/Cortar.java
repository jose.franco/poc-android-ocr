package com.example.imagetotext;

import android.app.Activity;

import java.util.ArrayList;
import java.util.List;

public class Cortar extends Activity {


    List<String> lista= new ArrayList<String>();
    private String texto;
    private String marca1;
    private String dominio1;
    private String modelo1;
    private String tipo1;
    private String uso1;
    private String chasis1;
    private String motor1;

    public Cortar(String texto) {
        this.texto=texto;
        remplazarDatos();
        cortar();
        this.dominio1=cortarDominio();
        this.marca1=cortarMarca();
        this.modelo1=cortarModelo();
        this.tipo1=cortarTipo();
        this.uso1=cortarUso();
        this.chasis1=cortarChasis();
        this.motor1=cortarMotor();

    }

    public String remplazarDatos(){
        String texto= this.texto;
        texto= texto.replace(' ',',');
        texto= texto.replace('\n',',');
        texto= texto.replace(":","");

        return texto;
    }
    //barra de acciones

    public void cortar(){
        String dominio=remplazarDatos();
        String out="";
        System.out.println(dominio+"[]");
        int i;
        char c;
        int j=20;
        while(j>0) {
            for (i=0; i<dominio.length(); i++){
                c=dominio.charAt(i);
                if(c==',') {
                    break;
                }
                out=out+c;
            }
            i=0;
            lista.add(out);
            //dominio=dominio.replace(out,"");
            //dominio=dominio.substring(out.length()+1,dominio.length());
            dominio=dominio.replace(out+",","");

            out="";
            j--;
        }


        //return dominio;
    }

    public String cortarDominio() {
        String dom="";
        String aux=lista.get(0);
        aux=aux.toLowerCase();
        int contador=0;
        char c;
        for (int i=0; i<aux.length(); i++){
            c=aux.charAt(i);
            if((i==0 && c=='d')|| (i==1 && c=='o') || (i==2 && c=='m') || (i==3 && c=='i') || (i==4 && c=='n') ||(i==5 && c=='i')|| (i==6 && c=='0') ) {
                contador=contador+1;
            }
        }
        if(contador>4) {
            dom=lista.get(1);
            lista.remove(0);
            lista.remove(0);

        }
        return dom;
    }

    public String cortarMarca() {
        String marca="";
        String aux=lista.get(0);
        aux=aux.toLowerCase();
        int contador=0;
        char c;
        for (int i=0; i<aux.length(); i++){
            c=aux.charAt(i);
            if((i==0 && c=='m')|| (i==1 && c=='a') || (i==2 && c=='r') || (i==3 && c=='c') || (i==4 && c=='a') ) {
                contador=contador+1;
            }
        }
        if(contador>2) {
            marca=lista.get(1);
            lista.remove(1);
            lista.remove(0);
        }
        return marca;
    }


    public String cortarModelo() {
        String modelo="";
        String aux=lista.get(0);
        aux=aux.toLowerCase();
        int contador=0;
        char c;
        for (int i=0; i<aux.length(); i++){
            c=aux.charAt(i);
            if((i==0 && c=='m')|| (i==1 && c=='o') || (i==2 && c=='d') || (i==3 && c=='e') || (i==4 && c=='l') || (i==5 && c=='o') ) {
                contador=contador+1;
            }
        }
        int j=0;
        boolean flag=false;
        int contador2=0;
        while(j<lista.size() && flag==false) {
            for (int i=0; i<lista.get(j).length(); i++){
                c=lista.get(j).charAt(i);
                if((i==0 && c=='T')|| (i==1 && c=='I') || (i==2 && c=='P') || (i==3 && c=='O')) {
                    contador2=contador2+1;
                }
                if(contador2>=2) {
                    flag=true;
                }
            }
            if(flag==false)
                j++;
        }
        if(contador>=3) {
            for(int i=1 ; i<j ;i++) {
                modelo=modelo+lista.get(i)+" ";
            }
            for(int i=0;j>0;j--) {
                lista.remove(0);
            }
        }
        return modelo;
    }



    public String cortarTipo() {
        String tipo="";
        String aux=lista.get(0);
        aux=aux.toLowerCase();
        int contador=0;
        char c;
        for (int i=0; i<aux.length(); i++){
            c=aux.charAt(i);
            if((i==0 && c=='t')|| (i==1 && c=='i') || (i==2 && c=='p') || (i==3 && c=='o')) {
                contador=contador+1;
            }
        }
        int j=0;
        boolean flag=false;
        int contador2=0;
        while(j<lista.size() && flag==false) {
            for (int i=0; i<lista.get(j).length(); i++){
                c=lista.get(j).charAt(i);
                if((i==0 && c=='U')|| (i==1 && c=='S') || (i==2 && c=='O')) {
                    contador2=contador2+1;
                }
                if(contador2>=2) {
                    flag=true;
                }
            }
            if(flag==false)
                j++;
        }
        if(contador>=3) {
            for(int i=1 ; i<j ;i++) {
                tipo=tipo+lista.get(i)+" ";
            }
            for(int i=0;j>0;j--) {
                lista.remove(0);
            }
        }
        return tipo;
    }



    public String cortarUso() {
        String uso="";
        String aux=lista.get(0);
        aux=aux.toLowerCase();
        int contador=0;
        char c;
        for (int i=0; i<aux.length(); i++){
            c=aux.charAt(i);
            if((i==0 && c=='u')|| (i==1 && c=='s') || (i==2 && c=='o') ) {
                contador=contador+1;
            }
        }
        if(contador>2) {
            uso=lista.get(1);
            lista.remove(1);
            lista.remove(0);
        }
        return uso;
    }


    public String cortarMotor() {
        String chasis="";
        boolean flag=false;
        int i=0;
        while(i<lista.size() &&flag==false) {
            if(lista.get(i).length()==9) {
                chasis=lista.get(i);
                flag=true;
            }
            i++;
        }
        return chasis;
    }


    public String cortarChasis() {
        String motor="";
        boolean flag=false;
        int i=0;
        while(i<lista.size() &&flag==false) {
            if(lista.get(i).length()>15) {
                motor=lista.get(i);
                flag=true;
            }
            i++;
        }
        return motor;
    }

    public String resultado(){
        return "marca=" + marca1 + ", dominio=" + dominio1 + ", modelo=" + modelo1 + ", tipo=" + tipo1
                + ", uso=" + uso1 + ", chasis=" + chasis1 + ", motor=" + motor1;
    }

    @Override
    public String toString() {
        return "clase [marca=" + marca1 + ", dominio=" + dominio1 + ", modelo=" + modelo1 + ", tipo=" + tipo1
                + ", uso=" + uso1 + ", chasis=" + chasis1 + ", motor=" + motor1 + "]";
    }
}
