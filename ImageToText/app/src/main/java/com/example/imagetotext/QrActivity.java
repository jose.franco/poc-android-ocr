package com.example.imagetotext;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.zxing.Result;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;





public class QrActivity extends AppCompatActivity  {

private Button btnScanner;
private TextView tvBarCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr);
        btnScanner= findViewById(R.id.btnScanner);
        tvBarCode= findViewById(R.id.tvBarCode);


        btnScanner.setOnClickListener(mOnClickListener);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data){
        super.onActivityResult(requestCode,resultCode,data);
        IntentResult result=IntentIntegrator.parseActivityResult(requestCode,resultCode,data);
        if(result!=null)
            if(result.getContents()!=null){
                tvBarCode.setText("El codigo es:"+result.getContents());
            }else{
                tvBarCode.setText("Error al escanear");
            }
    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener(){
    @Override
        public void onClick(View v){
            switch (v.getId()){
                case R.id.btnScanner:
                    new IntentIntegrator(QrActivity.this).initiateScan();
            }
        }
    };



}

